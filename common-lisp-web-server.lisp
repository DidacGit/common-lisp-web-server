;; Requires installing "libressl" through Guix and to customize the
;; "sbclrc" file
(ql:quickload :hunchentoot)
(ql:quickload :spinneret)
(defpackage :common-lisp-web-server
  (:use :cl :spinneret :hunchentoot))

(in-package :common-lisp-web-server)

;; Start my acceptor if it hasn't been started
(defvar +my-acceptor+ (make-instance 'easy-acceptor :port 8080))
(unless (started-p +my-acceptor+)
  (start +my-acceptor+))

(defmacro with-page ((&key title) &body body)
  "Expands to a typical html page with a TITLE and a BODY."
  `(with-html-string
       (:html
	(:head
	 (:title ,title))
	(:body
	 (:h1 ,title)
	 ,@body))))

;; Some example handlers for simple common web pages

;; GET request example
(define-easy-handler (show-query-params :uri "/show-query-params") ()
  (with-page (:title "Query Parameters")
    (if (get-parameters*)
	(:table :border 1
		(loop for (k . v) in (get-parameters*)
		      do (:tr (:td k) (:td v))))
	(:p "No query parameters."))))

;; HTML form to generate POST data example
(define-easy-handler (simple-form :uri "/simple-form") ()
  (with-page (:title "Simple Form")
    (:html
     (:form
      :method "POST" :action "/show-query-params"
      (:table
       (:tr (:td "Foo")
	    (:td (:input :name "foo" :size 20)))
       (:tr (:td "Password")
	    (:td (:input :name "password" :type "password" :size 20))))
      (:p (:input :name "submit" :type "submit" :value "Okay")
	  (:input ::type "reset" :value "Reset"))))))

;; Capture parameters individually
(define-easy-handler (random-number :uri "/random-number") ()
  (with-html-string
      (:html
       (:head (:title "Random"))
       (:body (:p (concatenate 'string "Random Number: "
			       (let ((query (parameter "limit"))) ; Returns string or nil
				 (write-to-string
				  (random (if query
					      (parse-integer query :junk-allowed t)
					      1000))))))))))

;; Show all the names and values of all the cookies sent by the browser
(define-easy-handler (show-cookies :uri "/show-cookies") ()
  (with-page (:title "Cookies")
    (let ((cookies (cookies-in*)))
      (if (null cookies)
	  (:p "No cookies.")
	  (:table (loop for (key . value) in cookies
			do (:tr (:td key) (:td value))))))))

;; Set a cookie
(define-easy-handler (set-a-cookie :uri "/set-a-cookie") ()
  (set-cookie "MyCookie" :value "A cookie value")
  (with-page (:title "Set Cookie")
    (:p "Cookie set.")
    (:p (:a :href "/show-cookies" "Look at the cookie jar."))))

;; -------------------------------
;; A small Application Framework
;; -------------------------------

(defmacro define-url-function (name params &body body)
  "Macro that abstracts Hunchentoot and Spinneret functionality."
  (let ((params (mapcar #'normalize-param params)))
    `(define-easy-handler (,name :uri ,(format nil "/~(~a~)" name)) ()
       (let (,@(param-bindings name params))
	 ,@(set-cookies-code name params)
	 (with-html-string ,@body)))))

(defun normalize-param (param)
  "Declaring a parameter with just a symbol is the same as declaring a
non-sticky string parameter with no default value."
  (etypecase param
    (list param)
    (symbol `(,param string nil nil))))

(defun param-bindings (function-name params)
  (loop for param in params
	collect (param-binding function-name param)))

(defun param-binding (function-name param)
  (destructuring-bind (name type &optional default sticky) param
    (let ((query-name (symbol->query-name name))
	  (cookie-name (symbol->cookie-name function-name name sticky)))
      `(,name (or
	       (string->type ',type (parameter ,query-name))
	       ,@(when cookie-name
		   (list `(string->type ',type
					(cookie-in ,cookie-name))))
	       ,default)))))

;; Convert strings obtained from the query parameters and cookies to
;; the desired type
(defgeneric string->type (type value))

;; Define at least a method specialized on the symbol string since that’s the default type.
;; Return string if it's not empty, otherwise return nil
(defmethod string->type ((type (eql 'string)) value)
  (and (plusp (length value)) value))

;; For integers return nil or its string
(defmethod string->type ((type (eql 'integer)) value)
  (parse-integer (or value "") :junk-allowed t))

(defun symbol->query-name (sym)
  (string-downcase sym))

(defun symbol->cookie-name (function-name sym sticky)
  (let ((package-name (package-name (symbol-package function-name))))
    (when sticky
      (ecase sticky
	(:global
	 (string-downcase sym))
	(:package
	 (format nil "~(~a:~a~)" package-name sym))
	(:local
	 (format nil "~(~a:~a:~a~)" package-name function-name sym))))))

(defun set-cookies-code (function-name params)
  (loop for param in params
	when (set-cookie-code function-name param) collect it))

(defun set-cookie-code (function-name param)
  (destructuring-bind (name type &optional default sticky) param
    (declare (ignore type default))
    (if sticky
	`(when ,name
	   (set-cookie
	    ,(symbol->cookie-name function-name name sticky)
	    :value (write-to-string ,name))))))

;; Example page
(define-url-function random-number2 ((limit integer 100))
  (:html
   (:head (:title "random"))
   (:body
    (:p (concatenate 'string
		     "Random number: " (write-to-string (random limit)))))))
